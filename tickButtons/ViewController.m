//
//  ViewController.m
//  tickButtons
//
//  Created by Jose Catala on 13/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnStatusAction:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    if ([btn.imageView.image isEqual:[UIImage imageNamed:@"tick"]])
    {
        btn.selected = NO;
    }
    else
    {
        btn.selected = YES;
    }
}

@end
