//
//  TickButton.m
//  tickButtons
//
//  Created by Jose Catala on 13/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "TickButton.h"

@implementation TickButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) setSelected:(BOOL)selected
{
    if (selected)
    {
        [self setImage:[UIImage imageNamed:@"tick"] forState:UIControlStateNormal];
    }
    else
    {
        [self setImage:[UIImage imageNamed:@"unTick"] forState:UIControlStateNormal];
    }
}

@end
